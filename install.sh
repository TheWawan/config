#!/usr/bin/env bash

# Define colors for prompt
RED='\033[0;32m'
NC='\033[0m' # No Color


# Update dependencies
echo "${RED}============================${NC}"
echo "${RED}Mise a jour des dependences ...${NC}"
echo "${RED}============================${NC}"
sudo apt update

# Terminal installation 
echo "${RED}============================${NC}"
echo "${RED}Installation du Terminal ...${NC}"
echo "${RED}============================${NC}"
sudo apt install -y terminator
sudo apt install -y zsh
sudo apt install -y curl
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
cp ./dependencies/terminal/.zshrc ~/.zshrc
sudo apt-get install fonts-powerline
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
echo "exec zsh" > ~/.bashrc
echo "${RED}============================${NC}"
echo "${RED}Zsh loaded ...${NC}"
echo "${RED}============================${NC}"

# Dev Dependencies
sudo apt install -y php7.2 php7.2-cli php7.2-common php7.2-curl php7.2-gd php7.2-json php7.2-mbstring php7.2-mysql php7.2-opcache php7.2-readline php7.2-sqlite3 php7.2-xml php7.2-zip

echo "${RED}============================${NC}"
echo "${RED}Installation de composer ...${NC}"
echo "${RED}============================${NC}"

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/bin --filename=composer
php -r "unlink('composer-setup.php');"


echo "${RED}============================${NC}"
echo "${RED}Installation de yarn ...${NC}"
echo "${RED}============================${NC}"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn


echo "${RED}============================${NC}"
echo "${RED}Installation de Nodejs ...${NC}"
echo "${RED}============================${NC}"
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

# Vim installation 
echo "${RED}============================${NC}"
echo "${RED}Installation de Vim  ...${NC}"
echo "${RED}============================${NC}"
sudo apt install -y vim 
cp dependencies/vim/.vimrc ~/.vimrc
cp -R dependencies/vim/.vim ~/.vim

# Fonts Installation
echo "${RED}============================${NC}"
echo "${RED}Ajout des fonts  ...${NC}"
echo "${RED}============================${NC}"
if [ ! -d "/usr/share/fonts" ];then
mkdir /usr/share/fonts
fi
sudo cp dependencies/fonts/DankMono-Italic.ttf  /usr/share/fonts
sudo cp dependencies/fonts/DankMono-Regular.ttf  /usr/share/fonts
fc-cache -f -v

echo "${RED}============================${NC}"
echo "${RED}Installation de sublime-text ...${NC}"
echo "${RED}============================${NC}"
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install -y apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text


echo "${RED}============================${NC}"
echo "${RED}Intervention utilisateur nescessaire ...${NC}"
echo "${RED}============================${NC}"

echo "${RED}============================${NC}"
echo "${RED}Lancer sublime et installer package manager${NC}"
echo "${RED}============================${NC}"

echo -n "Package manager installé ? (y/n)? "
read answer

if [ "$answer" != "${answer#[Yy]}" ] ;then
    cp -R  dependencies/sublime/Theme ~/.config/sublime-text-3/Packages/
    rm -rf ~/.config/sublime-text-3/Packages/User/
    git clone git@gitlab.com:r4z0rFr/Sublime.git ~/.config/sublime-text-3/Packages/User/
    sudo chmod -R 777 ~/.config/sublime-text/Packages/User/
else
    echo 'Vous devez installer package manager pour configurer sublime-text'
fi

echo "${RED}============================${NC}"
echo "${RED}Installation de spotify ...${NC}"
echo "${RED}============================${NC}"

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update
sudo apt-get install spotify-client

echo "${RED}============================${NC}"
echo "${RED}Spotify installé ...${NC}"
echo "${RED}============================${NC}"

echo "${RED}============================${NC}"
echo "${RED}Environnement Configuré, Enjoy ...${NC}"
echo "${RED}============================${NC}"




